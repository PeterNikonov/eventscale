## Composer install

composer require peter-nikonov/event-scale dev-master

---

The library is designed to analyze objects of events and arrange them on the timeline. For example, the timeline is from 06:00 to 10:00 with an interval of 15 minutes. The first thing that a library can do is to build a scale of a kind 06:00, 06:15, 06:30, 06:45, 07:00 ... 09:00, 09:30. The second - to put on the scale of the event, so that the time of the beginning of the event will be displayed on the scale. If the event duration closes the point on the timeline, it will be removed. If the end time of the event is not on the scale, it will be added on the scale.

This logic is covered by unit-tests, at the heart of the standard objects PHP - DateTime, DateInterval, DatePeriod.