<?php

use PeterNikonov\EventScale\MyEvent;
use PeterNikonov\EventScale\Processing;
use PeterNikonov\EventScale\TimeScale;
use PHPUnit\Framework\TestCase;

class EventTest extends TestCase
{
    /**
     * Test that event range calculate is correct work
     *
     * @return void
     */
    public function testEventPeriod()
    {
        $eventDate = '2040-01-01';
        $eventTime = '10:30';
        $eventDuration = 15;

        $event = new MyEvent();

        $event->setDate($eventDate);
        $event->setTime($eventTime);
        $event->setDuration($eventDuration);

        $processing = new Processing();
        $stamps = $processing->convertPeriodToArray($processing->getEventPeriod($event));
        $actual = array_pop($stamps);

        $this->assertEquals('10:44', $actual);
    }

    /**
     * Test that event range calculate is correct work
     *
     * @return void
     */
    public function testPutEventsOnScale()
    {
        $scaleStartTime = new DateTime('2040-01-01 08:00');
        $scaleEndTime = new DateTime('2040-01-01 20:00');
        $scaleInterval = new DateInterval('PT45M');

        $timeScale = new TimeScale($scaleStartTime, $scaleInterval, $scaleEndTime);

        $eventA = new MyEvent();
        $eventA->setDate('2040-01-01');
        $eventA->setTime('10:30');
        $eventA->setDuration(15);

        $eventB = new MyEvent();
        $eventB->setDate('2040-01-01');
        $eventB->setTime('12:30');
        $eventB->setDuration(35);

        $eventC = new MyEvent();
        $eventC->setDate('2040-01-01');
        $eventC->setTime('15:55');
        $eventC->setDuration(75);

        $eventD = new MyEvent();
        $eventD->setDate('2040-01-01');
        $eventD->setTime('07:55');
        $eventD->setDuration(25);

        $processing = new Processing();
        $scaleArray = $processing->putEventsOnScale($timeScale, [
            $eventA, $eventB, $eventC, $eventD
        ]);

        $this->assertFalse(in_array('08:00', $scaleArray));
        $this->assertFalse(in_array('16:15', $scaleArray));
        $this->assertTrue(in_array('08:20', $scaleArray));
        $this->assertTrue(in_array('10:45', $scaleArray));

    }
}
