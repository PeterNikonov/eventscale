<?php

namespace PeterNikonov\EventScale;

use DateInterval;
use DatePeriod;
use DateTime;

class Processing
{
    const ONE_SECOND_INTERVAL = 'PT1S';
    const TIME_FORMAT = 'H:i';

    /**
     * Gets event period.
     *
     * @param ScaleEvent $event
     * @return DatePeriod
     */
    public function getEventPeriod(ScaleEvent $event): DatePeriod
    {
        $interval = new DateInterval(self::ONE_SECOND_INTERVAL);

        return new DatePeriod($event->getDateTime(), $interval, $event->getEndTime());
    }

    /**
     *  Convert period to array, key is timestamp value is time
     *
     * @param DatePeriod $period
     * @return array
     */
    public function convertPeriodToArray(DatePeriod $period): array
    {
        /**
         * @var $period DateTime[]
         */
        $result = [];
        foreach ($period as $dt) {
            $stamp = $dt->getTimestamp();
            $result[$stamp] = $dt->format(self::TIME_FORMAT);
        }

        return $result;
    }

    /**
     * Put events on time scale.
     *
     * @param TimeScale $scale
     * @param ScaleEvent[] $events
     * @return array
     */
    public function putEventsOnScale(TimeScale $scale, array $events): array
    {
        /**
         * @var $period DateTime[]
         */
        $period = $scale->getPeriod();
        $result = [];

        foreach ($period as $point) {
            $stamp = $point->getTimestamp();
            $result[$stamp] = $point->format(self::TIME_FORMAT);
        }

        foreach ($events as $event) {
            $stamp = $event->getDateTime()->getTimestamp();
            $result[$stamp] = $event->getDateTime()->format(self::TIME_FORMAT);

            if ($event->getEndTime() < $scale->getEndTime()) {
                $stamp = $event->getEndTime()->getTimestamp();
                $result[$stamp] = $event->getEndTime()->format(self::TIME_FORMAT);
            }

            /**
             * @var $eventPeriod DateTime[]
             */
            $eventPeriod = $this->getEventPeriod($event);
            foreach ($eventPeriod as $point) {
                if ($point == $event->getDateTime()) {
                    continue;
                }
                $stamp = $point->getTimestamp();
                unset($result[$stamp]);
            }
        }

        ksort($result);

        return $result;
    }
}
