<?php

namespace PeterNikonov\EventScale;

use DateInterval;
use DatePeriod;
use DateTime;

class TimeScale
{
    private $startTime;
    private $endTime;
    private $interval;

    public function __construct(DateTime $startTime, DateInterval $interval, DateTime $endTime)
    {
        $this->startTime = $startTime;
        $this->endTime = $endTime;
        $this->interval = $interval;
    }

    public function getPeriod()
    {
        return new DatePeriod($this->startTime, $this->interval, $this->endTime);
    }

    /**
     * @return DateTime
     */
    public function getStartTime(): DateTime
    {
        return $this->startTime;
    }

    /**
     * @param DateTime $startTime
     */
    public function setStartTime(DateTime $startTime)
    {
        $this->startTime = $startTime;
    }

    /**
     * @return DateTime
     */
    public function getEndTime(): DateTime
    {
        return $this->endTime;
    }

    /**
     * @param DateTime $endTime
     */
    public function setEndTime(DateTime $endTime)
    {
        $this->endTime = $endTime;
    }

    /**
     * @return DateInterval
     */
    public function getInterval(): DateInterval
    {
        return $this->interval;
    }

    /**
     * @param DateInterval $interval
     */
    public function setInterval(DateInterval $interval)
    {
        $this->interval = $interval;
    }

}