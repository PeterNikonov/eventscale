<?php

namespace PeterNikonov\EventScale;

use DateInterval;
use DateTime;

class MyEvent extends ScaleEvent
{
    protected $date;
    protected $time;
    protected $duration;

    protected function makeDateTime(): DateTime
    {
        return new DateTime($this->date . ' ' . $this->time);
    }

    protected function makeInterval(): DateInterval
    {
        $intervalSpec = sprintf("PT%dM", $this->duration);
        return new DateInterval($intervalSpec);
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param string $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param string $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }
}
