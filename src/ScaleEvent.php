<?php

namespace PeterNikonov\EventScale;

use DateInterval;
use DateTime;

abstract class ScaleEvent
{
    /**
     * @var DateTime
     */
    private $dateTime;
    /**
     * @var DateInterval
     */
    private $interval;

    abstract protected function makeDateTime(): DateTime;

    abstract protected function makeInterval(): DateInterval;

    public function getDateTime(): DateTime
    {
        if (empty($this->dateTime)) {
            $this->dateTime = $this->makeDateTime();
        }

        return $this->dateTime;
    }

    public function getInterval(): DateInterval
    {
        if (empty($this->interval)) {
            $this->interval = $this->makeInterval();
        }

        return $this->interval;
    }

    public function getEndTime(): DateTime
    {
        return (clone $this->getDateTime())->add($this->getInterval());
    }
}
